<?php
/*
Plugin Name: Geo My Wp custom form
Description: Geo My Wp custom form
Version: 1.0.0
Plugin URI: https://www.fiverr.com/wp_right
Author: LogicsBuffer
Author URI: http://logicsbuffer.com/
*/

add_action('wp_enqueue_scripts', 'geo_my_wp_script_front_css');
add_action('wp_enqueue_scripts', 'geo_my_wp_script_front_js');
add_action('admin_enqueue_scripts', 'geo_my_wp_script_back_css');
add_action('init', 'wp_astro_init');
function wp_astro_init() {
   add_shortcode('show_geomywp_form', 'wp_geo_my_wp_form');
}
function wp_geo_my_wp_form($atts) {
	$forum_id =$atts['forum_id'];
	if(isset($_POST['submit_geo_wp'])){
		$street = '';
		$locality = '';
		$state = '';
		$country = '';
		$postal_code = '';
		$rt_name = $_POST['rt_name'];
		$rt_desc = $_POST['rt_desc'];
		$street_number = $_POST['street_number'];
		$route = $_POST['route'];
		$rt_tags_str = $_POST['rt_tags'];
		$rt_tags = explode(",",$rt_tags_str);
		
		if($street_number || $route){
		$street = $street_number.' '.$route;
		}
		if($_POST['locality']){
		$locality = $_POST['locality'];
		}
		if($_POST['locality']){
		$locality = $_POST['locality'];
		}
		if($_POST['country']){
		$country = $_POST['country'];
		}
		if($_POST['state']){
		$state = $_POST['state'];
		}
		if($_POST['postal_code']){
		$postal_code = $_POST['postal_code'];
		}
		//$postal_code = $_POST['postal_code'];
		$my_post = array(
		  'post_title'    		 => $rt_name,
		  'post_content' 		 => $rt_desc,
		  'post_status'          => 'publish',
		  'post_type'            => 'topic'
		  );
		// Insert the post into the database
		$post_id = wp_insert_post( $my_post );
		wp_set_object_terms( $post_id, $rt_tags, 'topic-tag');

		 /* @author Eyal Fitoussi
 *
 * @param  integer          $post_id  int ( post ID, user ID, comment ID... )
 * @param  string || array  $location to pass an address it can be either a string or an array of address field for example:
 *
 * $location = array(
 *      'street'    => 285 Fulton St,
 *      'apt'       => '',
 *      'city'      => 'New York',
 *      'state'     => 'NY',
 *      'zipcode'   => '10007',
 *      'country'   => 'USA'
 * );
 *
 * or pass a set of coordinates via an array of lat,lng. Ex
 *
 * $location = array(
 *     'lat' => 26.1345,
 *     'lng' => -80.4362
 * );
 *
 * @param  integer $user_id      the user whom the location belongs to. By default it will belong to the user who creates/update the post location ( logged in user ).
 * @param  boolean $force_refresh false to use geocoded address in cache || true to force address geocoding
 *
 * @return int location ID
 */
 $location = array(
'street'    => $street,
'apt'       => '',
'city'      => $locality,
'state'     => $state,
'zipcode'   => $postal_code,
'country'   => $country
 );
 $user_id = 2;
 if ( is_user_logged_in() ) {
	$user_id = get_current_user_id();
 }
gmw_update_post_location( $post_id, $location, $user_id , false );
//$forum_id = 6813;
$anonymous_data = bbp_filter_anonymous_post_data();
$author_id      = bbp_get_topic_author_id( $topic_id );
$is_edit        = (bool) isset( $_POST['save'] );

// Formally update the topic
bbp_update_topic( $post_id, $forum_id, $anonymous_data, $author_id, $is_edit );
do_action( 'bbp_topic_attributes_metabox_save', $post_id, $forum_id);
?>
		<div class="alert alert-success">
								</div><?php
	}
	?>
	<!-- Replace the value of the key parameter with your own API key. -->
<form role="form" action="" method="post" id="addmenu" method="post">
								<div class="form-group">
									<label class="control-label" for="address">Topic title:</label>
									<input   placeholder="Enter Dish Name here" style="background-color:white;" type="text" name="rt_name" class="form-control" id="name" required>
								</div>
								<div class="form-group">
									<label class="control-label" for="dish">Topic Description:</label>
									<textarea  placeholder="Enter Description of your Dish here" col="10" rows="8" style="background-color:white;" type="text" name="rt_desc" class="form-control" id="dish" required></textarea>
								</div>

                <div class="form-group">
								<label class="control-label" for="address">Topic Tags:</label><br/>
								<input placeholder="Enter topic tags here" style="background-color:white;" type="text" name="rt_tags" class="form-control" id="name" >
								<div style="color: gray;margin-top: 5px;margin-left: 2px;">(Separate tags with commas)</div>
				</div>

                <div class="form-group" id="locationField">
				<input id="autocomplete" placeholder="Enter your address" onfocus="geolocate()" type="text" required="" autocomplete="off" style="
    width: 100%;">
                </div>

<div id="address" class="hide">

   <div class="form-group">
      <label class="control-label">Street address</label>
	    <input class="field form-control" name="street_number" type="text" id="street_number" disabled="true"></input>
   </div>

   <div class="form-group">
      <label class="control-label">Route</label>
	    <input class="field" name="route" id="route" type="text" disabled="true"></input>
   </div>

   <div class="form-group">
      <label class="control-label">City</label>
      <input class="field" id="locality" name="locality" type="text" disabled="true"></input>
   </div>

   <div class="form-group">
      <label class="control-label">State</label>
      <input class="field" name="state" id="administrative_area_level_1" type="text" disabled="true"></input>
   </div>

   <div class="form-group">
      <label class="control-label">Zip code</label>
      <input class="field" name="postal_code" id="postal_code" type="text" disabled="true"></input>
   </div>

   <div class="form-group">
      <label class="control-label">Country</label>
      <input class="field" name="country" id="country" type="text" disabled="true"></input>
   </div>

</div>
 <div class="form-group">
								<input type="submit" name="submit_geo_wp" value="Submit Topic" class="btn btn-primary btn-block">
								</div>
							</form>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA2OAM_YIffl7IV-TUwzEjzU9JWGnFp0nc&libraries=places&callback=initAutocomplete"
        async defer></script>
	<?php
}
function geo_my_wp_script_back_css() {

}

function geo_my_wp_script_front_css() {
		/* CSS */
        wp_register_style('geo_my_wp_style', plugins_url('css/geo_my_wp.css',__FILE__));
        wp_enqueue_style('geo_my_wp_style');
}

		add_action( 'wp_ajax_my_ajax_rt', 'my_ajax_rt' );
		add_action( 'wp_ajax_nopriv_my_ajax_rt', 'my_ajax_rt' );
function my_ajax_rt() {

}

function geo_my_wp_script_back_js() {

}



function geo_my_wp_script_front_js() {


        wp_register_script('geo_my_wp_script', plugins_url('js/geo_my_wp.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('geo_my_wp_script');

}
